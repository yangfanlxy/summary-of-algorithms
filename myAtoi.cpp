#include <iostream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <cmath>
#include <cctype>
#include "stdint.h"

int myAtoi(const char* str)
{
    if (nullptr == str || strlen(str) < 1)
    {
        throw new std::runtime_error("Invalid parameters");
    }

    int64_t maxInt = pow(2, 31) - 1;
    int64_t minInt = - pow(2, 31);
    int64_t result = 0;
    int sign = 1;

    while(isspace(*str))
    {
        str++;
    }

    if ('-' == *str || '+' == *str)
    {
        sign = '-' == *str ? -1 : 1;
        str++;
    }

    while('\0' != *str)
    {
        if (!isdigit(*str))
        {
            throw new std::runtime_error("Invalid parameters");
        }

        result = *str - '0' + result * 10;
        str++;

        if (sign * result > maxInt || sign * result < minInt)
        {
            throw new std::runtime_error("Invalid parameters");
        }
    }

    return sign * result;
}

int main()
{
    std::string input;
    std::cin >> input;
    std::cout << myAtoi(input.c_str()) << std::endl;
    return 0;
}

/*
few problems :
1. wrong use maxInt to instead of minInt
2. forget using symbol bit, for negative and positive
3. 从前往后数字的组合方法
*/