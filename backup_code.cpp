#include <stack>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
	/**
	* @param expression: a vector of strings;
	* @return: an integer
	*/

	void checkPlusSubOperation(stack<double>& numStack, stack<char>& opStack)
	{
		if (opStack.empty() || opStack.top() != '+' && opStack.top() != '-')
			return;

		double num2 = numStack.top();
		numStack.pop();
		double num1 = numStack.top();
		numStack.pop();

		double result = opStack.top() == '+' ? num1 + num2 : num1 - num2;
		opStack.pop();
		numStack.push(result);
	}

	void checkMulDivOperation(stack<double>& numStack, stack<char>& opStack)
	{
		if (!opStack.empty() && opStack.top() == '-')
		{
			double temp = -numStack.top();
			numStack.pop();
			numStack.push(temp);
			opStack.pop();
			opStack.push('+');
		}

		if (opStack.empty() || opStack.top() != '*' && opStack.top() != '/')
			return;

		double num2 = numStack.top();
		numStack.pop();
		double num1 = numStack.top();
		numStack.pop();

		double result = opStack.top() == '*' ? num1 * num2 : num1 / num2;
		opStack.pop();
		numStack.push(result);
	}

	int evaluateExpression2(vector<string> &expression) {
		// write your code here
		if (expression.empty())
			return 0;

		std::stack<double> numStack;
		std::stack<char> opStack;
		for (std::string& str : expression)
		{
			if (isdigit(str[0]) || str.size() > 1)
			{
				numStack.push(stoi(str));
				checkMulDivOperation(numStack, opStack);
			}
			else
			{
				if (str[0] == ')')
				{
					while (opStack.top() != '(')
						checkPlusSubOperation(numStack, opStack);

					opStack.pop();
					checkMulDivOperation(numStack, opStack);
				}
				else
					opStack.push(str[0]);
			}
		}
		while (!opStack.empty())
			checkPlusSubOperation(numStack, opStack);
		return (int)(numStack.top());
	}

	bool curPrecedeLast(char cur, char last)
	{
		if (last == '(')
			return true;

		return (cur == '*' || cur == '/')
			&& (last == '+' || last == '-');
	}

	void calc(stack<int>& numStack, stack<char>& opStack)
	{
		int num2 = numStack.top();
		numStack.pop();
		int num1 = numStack.top();
		numStack.pop();

		switch (opStack.top())
		{
		case '+':
			numStack.push(num1 + num2);
			break;
		case '-':
			numStack.push(num1 - num2);
			break;
		case '*':
			numStack.push(num1 * num2);
			break;
		default:
			numStack.push(num1 / num2);
			break;
		}
		opStack.pop();
	}

	int evaluateExpression(vector<string> &expression) {
		// write your code here

		std::stack<int> numStack;
		std::stack<char> opStack;
		for (std::string& str : expression)
		{
			if (isdigit(str[0]) || str.size() > 1)
			{
				numStack.push(stoi(str));
			}
			else
			{
				if (str[0] == ')')
				{
					while (opStack.top() != '(')
						calc(numStack, opStack);
					opStack.pop();
				}
				else if (str[0] == '(')
					opStack.push(str[0]);
				else
				{
					if (!opStack.empty() && !curPrecedeLast(str[0], opStack.top()))
						calc(numStack, opStack);
					opStack.push(str[0]);
				}
			}
		}

		while (!opStack.empty())
			calc(numStack, opStack);

		return numStack.empty() ? 0 : numStack.top();
	}
};

template <class RandomAccessIterator>
RandomAccessIterator partition(RandomAccessIterator begin, RandomAccessIterator end)
{
	auto resultIter = begin;
	auto back = end - 1;
	for (auto iter = begin; iter != back; ++iter)
	{
		if (*iter <= *back)
		{
			std::swap(*iter, *resultIter); 
			resultIter++;
		}
	}
	std::swap(*resultIter, *back);
	return resultIter;
}

//sort [begin, end)
template <class RandomAccessIterator>
void mySort(RandomAccessIterator begin, RandomAccessIterator end)
{
	if (end - begin > 1)
	{
		auto pivot = partition(begin, end);
		mySort(begin, pivot);
		mySort(pivot + 1, end);
	}
}

void merge(vector<int>& num, int begin, int mid, int end)
{
	vector<int> result;
	int left = begin;
	int right = mid;
	for (int i = begin; i != end; ++i)
	{
		if (left < mid && (right >= end || num[left] <= num[right]))
		{
			result.push_back(num[left]);
			left++;
		}
		else
		{
			result.push_back(num[right]);
			right++;
		}
	}

	auto iter = result.begin();
	for (int i = begin; i != end; ++i)
	{
		num[i] = *iter;
		iter++;
	}
}

//sort [begin, end)
void myMergeSort(vector<int>& num, int begin, int end)
{
	if (end - begin > 1)
	{
		int mid = (begin + end) >> 1;
		myMergeSort(num, begin, mid);
		myMergeSort(num, mid, end);

		merge(num, begin, mid, end);
	}
}

void partition(vector<int>& nums)
{
	int negaIndex = 0;
	int zeroIndex = nums.size();
	for (int i = 0; i < zeroIndex;)
	{
		if (nums.at(i) == 0)
		{
			zeroIndex--;
			std::swap(nums[i], nums[zeroIndex]);
		}
		else if (nums.at(i) < 0)
		{
			std::swap(nums[i], nums[negaIndex]);
			negaIndex++;
			i++;
		}
		else
			i++;
	}
	std::rotate(nums.begin() + negaIndex
		, nums.begin() + zeroIndex
		, nums.end());
}

void updateHeap(vector<int>& nums, int length)
{
	int index = 0;
	while (index < length)
	{
		int left = index * 2 + 1;
		int right = index * 2 + 2;

		if (right < length && std::max(nums[left], nums[right]) > nums[index])
		{
			int next = nums[left] >= nums[right] ? left : right;
			std::swap(nums[next], nums[index]);
			index = next;
		}
		else if (left < length && nums[left] > nums[index])
		{
			std::swap(nums[left], nums[index]);
			index = left;
		}
		else
			break;
	}
}

void myHeapSort(vector<int>& nums)
{
	int length = nums.size();
	for (int i = 1; i != length; ++i)
	{
		int index = i;
		while (index >= 1) 
		{
			int parent = (index - 1) >> 1;
			if (nums[index] > nums[parent])
			{
				std::swap(nums[index], nums[parent]);
				index = parent;
			}
			else
				break;
		}
		
	}

	for (int i = length - 1; i > 0; --i)
	{
		std::swap(nums[0], nums[i]);
		updateHeap(nums, i);
	}
}

int is_square(int n) {
	int temp = (int)sqrt(n);
	return temp * temp == n;
}

int maxProduct(vector<int>& nums) {
	int res = nums.size() > 0 ? nums.front() : 0;
	int _max = res;
	int _min = res;
	int len = nums.size();
	for (int i = 1; i < len; ++i)
	{
		int temp = _max;
		int num = nums.at(i);
		_max = std::max(num, max(_max * num, _min * num));
		_min = std::min(num, min(temp * num, _min * num));
		res = std::max(res, _max);
	}
	return res;
}

string shortestPalindrome(string s) {
	int n = s.size();
	int arraySize = n + 1;
	int* dp = new int[arraySize * arraySize + arraySize];

	for (int i = 0; i != arraySize * arraySize + arraySize; ++i) dp[i] = 0;

	int _maxCommon = 0;
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
		{
			char ch1 = s.at(i - 1);
			char ch2 = s.at(n - j);
			if (ch1 == ch2)
				dp[i * n + j] = dp[(i - 1) * n + j - 1] + 1;

			if (dp[i * n + j] > _maxCommon && dp[i * n + j] == i)
				_maxCommon = dp[i * n + j];
		}

	string pre;
	for (int i = _maxCommon; i != n; ++i)
		pre.push_back(s.at(i));

	delete[] dp;
	return pre + s;
}

int partition(vector<int>::iterator begin, vector<int>::iterator end, int resultNum)
{
	vector<int> test;
	for (auto iter = begin; iter != end; ++iter) test.push_back(*iter);
	int dis = rand() % (end - begin);
	auto mid = begin + dis;
	auto i = begin;
	auto j = begin;
	auto last = end - 1;
	swap(*mid, *last);
	for (; j < last; ++j)
	{
		if (*j < *last)
		{
			swap(*i, *j);
			++i;
		}
	}
	swap(*i, *last);

	test.clear();
	for (auto iter = begin; iter != end; ++iter) test.push_back(*iter);

	if (i - begin + 1 < resultNum)
	{
		return partition(i + 1, end, resultNum - (i - begin + 1));
	}
	else if (i - begin + 1 == resultNum)
	{
		return *i;
	}
	return partition(begin, i + 1, resultNum);
}

int median(vector<int> &nums) {
	// write your code here
	int size = nums.size();
	int resultNum = size % 2 == 0 ? size / 2 : size / 2 + 1;
	return partition(nums.begin(), nums.end(), resultNum);
}

int main()
{
	/*vector<string> exs = { "(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")","+","(","50","+","4","*","3","/","2","+","799","-","180","+","9","+","8","+","(","3","/","3",")","+","8","+","(","9","+","3",")","/","3",")"

	};

	Solution sl;
	int result = sl.evaluateExpression(exs);*/

	//vector<int> list = {2, 1, 3, 2, 5, 6, 2, 0, 9, 8, 3, 5, 7};
	//vector<int> list = {1, -1};
	//mySort(list.begin(), list.end());

	//myMergeSort(list, 0, list.size());

	//vector<int> nums = {0};
	//vector<int> nums = { 1 };
	//vector<int> nums = {-1 };
	//vector<int> nums = { 0, 0};
	//vector<int> nums = { 1, 1 };
	//vector<int> nums = { -1, -1 };
	//vector<int> nums = { 0, 1 };
	//vector<int> nums = { 1, 0 };
	//vector<int> nums = { 0, -1 };
	//vector<int> nums = { -1, 0 };
	//vector<int> nums = { 1, -1 };
	//vector<int> nums = { -1, 1 };
	//vector<int> nums = { -1, 0 , 1};
	//vector<int> nums = { -1, 1 , 0 };
	//vector<int> nums = { 0, -1 , 1 };
	//vector<int> nums = { 0, 1 , -1 };
	//vector<int> nums = { 1, -1 , 0 };
	//vector<int> nums = { 1, 0 , -1 , 1, 2, 4, 0, 8, 7, 6 ,7 ,8 ,9 , 11, 3 ,44 ,5 ,66 ,7, 8 ,9};
	//myHeapSort(nums);
	//partition(nums);

	//string input("abcd");

	//string result = shortestPalindrome(input);

	//int r = 5;

	vector<int> nums = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-2,1,-15,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	int result = median(nums);

	return 0;
}